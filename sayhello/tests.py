from django.test import TestCase

class IndexTestCase(TestCase):
	def test_check_hello_world_is_printed(self):
		response = self.client.get('/')
		self.assertContains(response, 'hello world')
